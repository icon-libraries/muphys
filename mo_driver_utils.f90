!>
!! @brief Module containing array allocation/loading utility subroutines
!!
!! The code is largely copied from Serialbox, with some modifications
!!
!! @contact C.Bignamini (bignamini@cscs.ch)
!!
MODULE mo_driver_utils

  USE mo_kind, ONLY: wp
  USE m_serialize, ONLY : t_serializer, fs_get_size, fs_get_halos, fs_field_exists

  IMPLICIT NONE

  PRIVATE
  
!  TODO: create interface
!  INTERFACE allocate_allocatable
!          allocate_allocatable_real_1d, &
!          allocate_allocatable_real_2d
!  END INTERFACE allocate_allocatable

  PUBLIC :: allocate_allocatable_real_1d
  PUBLIC :: allocate_allocatable_real_2d
  
CONTAINS

  ! ftg_allocate_and_read_allocatable is not usable because it generates the wrong
  ! dimensions during the allocation
  SUBROUTINE allocate_allocatable_real_1d(serializer, fieldname, field)
  !-------------------------------------------------------------------------------
  !
  ! Description:
  !   This routine allocates a 1d array (field) according to the size found using
  !   the provided serializer and fieldname. All array element are set to zero. No
  !   This is an allocation only subroutine, no data reading is performed.
  !
  !-------------------------------------------------------------------------------  
    TYPE(t_serializer), INTENT(IN), POINTER      :: serializer
    CHARACTER(LEN=*), INTENT(IN)                 :: fieldname   
    REAL(wp), INTENT(OUT), ALLOCATABLE           :: field(:)    
    INTEGER, DIMENSION(8)                        :: bounds      
    INTEGER, DIMENSION(4)                        :: field_size
    
    IF (ALLOCATED(field)) THEN                                  
       DEALLOCATE(field)                                         
    END IF
    IF (fs_field_exists(serializer, fieldname)) THEN
       field_size = fs_get_size(serializer,  fieldname)
       ALLOCATE(field(field_size(1)))
    END IF
    field = 0.d0

  END SUBROUTINE allocate_allocatable_real_1d

  ! ftg_allocate_and_read_allocatable is not usable because it generates the wrong
  ! dimensions during the allocation
  SUBROUTINE allocate_allocatable_real_2d(serializer, fieldname, field)
  !-------------------------------------------------------------------------------
  !
  ! Description:
  !   This routine allocates a 2d array (field) according to the size found using
  !   the provided serializer and fieldname. All array element are set to zero. No
  !   This is an allocation only subroutine, no data reading is performed.
  !
  !-------------------------------------------------------------------------------  
    TYPE(t_serializer), INTENT(IN), POINTER      :: serializer
    CHARACTER(LEN=*), INTENT(IN)                 :: fieldname   
    REAL(wp), INTENT(OUT), ALLOCATABLE           :: field(:,:)    
    INTEGER, DIMENSION(8)                        :: bounds      
    INTEGER, DIMENSION(4)                        :: field_size
    
    IF (ALLOCATED(field)) THEN                                  
       DEALLOCATE(field)                                         
    END IF
    IF (fs_field_exists(serializer, fieldname)) THEN                       
       field_size = fs_get_size(serializer,  fieldname)
       ALLOCATE(field(field_size(1),field_size(2)))
    END IF
    field = 0.d0

  END SUBROUTINE allocate_allocatable_real_2d
  
END MODULE mo_driver_utils

