#!/bin/bash
set -x

# simple test for cf and non-cf output

ifile=$1
mode=$2

meshsize=$(cdo -s -ngridpoints -selname,ta ${ifile})

# mode should be orig or cf
if [[ 'cf' = "${mode}" ]] ; then
  [[ 160 -eq ${meshsize} ]] || exit 1
else
  [[ 11200 -eq ${meshsize} ]] || exit 1
fi
exit 0
