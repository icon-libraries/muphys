PROGRAM data_checker

  USE m_ser_ftg_cmp
  USE m_serialize, ONLY : t_serializer, t_savepoint, fs_create_serializer, fs_destroy_serializer, &
       fs_create_savepoint, fs_destroy_savepoint, fs_read_field, fs_field_exists, fs_get_size, fs_get_halos
  USE mo_kind
  USE mo_driver_utils
  
  IMPLICIT NONE

  CHARACTER(LEN=:), ALLOCATABLE :: directory, base_name_reference, base_name_new
  TYPE(t_serializer), POINTER :: serializer
  TYPE(t_savepoint), POINTER :: savepoint

  ! Fields loaded from "new" input dataset (directory/base_name_new, Serialbox savepoint: graupel_run_out)
  REAL(wp), ALLOCATABLE :: pflx_new(:, :)
  REAL(wp), ALLOCATABLE :: prr_gsp_new(:), pri_gsp_new(:), prs_gsp_new(:), prg_gsp_new(:)

  ! Comparison status variable
  LOGICAL :: result
  INTEGER :: failure_count

  ! Command line input parsing
  LOGICAL :: error_status
  error_status = .FALSE.
  CALL parse_args(directory, base_name_reference, base_name_new, error_status)
  IF(error_status) THEN
     RETURN
  END IF

!!! NEW DATASET LOADING

  PRINT*,'Loading ',base_name_new,' dataset'
  ALLOCATE(serializer) 
  ALLOCATE(savepoint)
  CALL fs_create_serializer(directory, base_name_new, 'r', serializer)
  CALL fs_create_savepoint('graupel_standalone_run_out', savepoint)
  CALL allocate_allocatable_real_2d(serializer, "pflx", pflx_new)
  CALL fs_read_field(serializer, savepoint, "pflx", pflx_new)
  CALL allocate_allocatable_real_1d(serializer, "prr_gsp", prr_gsp_new)
  CALL fs_read_field(serializer, savepoint, "prr_gsp", prr_gsp_new)
  CALL allocate_allocatable_real_1d(serializer, "pri_gsp", pri_gsp_new)
  CALL fs_read_field(serializer, savepoint, "pri_gsp", pri_gsp_new)
  CALL allocate_allocatable_real_1d(serializer, "prs_gsp", prs_gsp_new)
  CALL fs_read_field(serializer, savepoint, "prs_gsp", prs_gsp_new)
  CALL allocate_allocatable_real_1d(serializer, "prg_gsp", prg_gsp_new)
  CALL fs_read_field(serializer, savepoint, "prg_gsp", prg_gsp_new)
  CALL fs_destroy_savepoint(savepoint)
  CALL fs_destroy_serializer(serializer)

!!! COMPARISON WITH REFERENCE DATA

  PRINT*,'Loading ',base_name_reference,' dataset and running check'
  CALL fs_create_serializer(directory, base_name_reference, 'r', serializer)
  CALL fs_create_savepoint('graupel_run_out', savepoint)

  result = .FALSE.        
  failure_count = 0       
  CALL compare_real_2d(serializer, savepoint, "pflx", pflx_new, result, failure_count, fieldname_alias="pflx_new")
  CALL compare_real_1d(serializer, savepoint, "prr_gsp", prr_gsp_new, result, failure_count, fieldname_alias="prr_gsp_new")
  CALL compare_real_1d(serializer, savepoint, "pri_gsp", pri_gsp_new, result, failure_count, fieldname_alias="pri_gsp_new")
  CALL compare_real_1d(serializer, savepoint, "prs_gsp", prs_gsp_new, result, failure_count, fieldname_alias="prs_gsp_new")
  CALL compare_real_1d(serializer, savepoint, "prg_gsp", prg_gsp_new, result, failure_count, fieldname_alias="prg_gsp_new")
  
  CALL fs_destroy_savepoint(savepoint)
  CALL fs_destroy_serializer(serializer)
  
CONTAINS

  SUBROUTINE parse_args(data_directory, base_name_reference, base_name_new, error_status)
    CHARACTER(LEN=:), ALLOCATABLE, INTENT(OUT) :: data_directory, base_name_reference, base_name_new
    INTEGER :: nargs, arg_len
    LOGICAL, INTENT(INOUT) :: error_status
    
    nargs = command_argument_count()

    IF (nargs < 3) THEN
       PRINT*, "Error, data directory and/or base names not provided. The data checker execution command is:"
       PRINT*, "./data_checker <path_to_datasets_folder> <reference_dataset_base_name> <new_dataset_base_name>"
       error_status = .TRUE.
       RETURN
    END IF
    
    CALL get_command_argument(1, length=arg_len)
    ALLOCATE(CHARACTER(arg_len) :: data_directory)
    CALL get_command_argument(1, data_directory)
    WRITE(*, '(a, a)') "data directory: ", data_directory

    CALL get_command_argument(2, length=arg_len)
    ALLOCATE(CHARACTER(arg_len) :: base_name_reference)
    CALL get_command_argument(2, base_name_reference)
    WRITE(*, '(a, a)') "base_name_reference: ", base_name_reference

    CALL get_command_argument(3, length=arg_len)
    ALLOCATE(CHARACTER(arg_len) :: base_name_new)
    CALL get_command_argument(3, base_name_new)
    WRITE(*, '(a, a)') "base_name_new: ", base_name_new

  END SUBROUTINE parse_args

  FUNCTION cmp_bounds(serializer, fieldname, lbounds, ubounds, fieldname_print)

    TYPE(t_serializer), INTENT(IN), POINTER      :: serializer
    CHARACTER(LEN=*), INTENT(IN) :: fieldname, fieldname_print
    INTEGER, INTENT(IN) :: lbounds(:), ubounds(:)
    INTEGER :: rank, expected_bounds(2, 4), r
    LOGICAL :: cmp_bounds

    rank = SIZE(lbounds)
    expected_bounds = RESHAPE(fs_get_halos(serializer, fieldname), (/2, 4/))
    cmp_bounds = ALL(lbounds == expected_bounds(1,:rank)) .AND. ALL(ubounds == expected_bounds(2,:rank))

    IF (.NOT. cmp_bounds .AND. .NOT. ftg_cmp_quiet) THEN
       WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : Bounds don't match"
       WRITE (*,'(A)',advance="no") "  -> expected: ("
       DO r = 1, rank
          IF (r > 1) THEN
             WRITE(*,'(A)',advance="no") ', '
          END IF
          WRITE (*,'(I0)',advance="no") expected_bounds(1,r)
          WRITE (*,'(A)',advance="no") ':'
          WRITE (*,'(I0)',advance="no") expected_bounds(2,r)
       END DO
       WRITE (*,'(A)',advance="no") "), actual: ("
       DO r = 1, rank
          IF (r > 1) THEN
             WRITE(*,'(A)',advance="no") ', '
          END IF
          WRITE (*,'(I0)',advance="no") lbounds(r)
          WRITE (*,'(A)',advance="no") ':'
          WRITE (*,'(I0)',advance="no") ubounds(r)
       END DO
       WRITE (*,'(A)') ")"
    END IF

  END FUNCTION cmp_bounds

  FUNCTION cmp_size(serializer, fieldname, actual_shape, fieldname_print)
    TYPE(t_serializer), INTENT(IN), POINTER :: serializer
    CHARACTER(LEN=*), INTENT(IN) :: fieldname, fieldname_print
    INTEGER, INTENT(IN) :: actual_shape(:)
    INTEGER :: rank, expected_shape(4), r
    LOGICAL :: cmp_size

    rank = SIZE(actual_shape)
    expected_shape = fs_get_size(serializer, fieldname)
    cmp_size = ALL(actual_shape == expected_shape(:rank))

    IF (.NOT. cmp_size .AND. .NOT. ftg_cmp_quiet) THEN
       WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : Size doesn't match"
       WRITE (*,'(A)',advance="no") "  -> expected: ("
       DO r = 1, rank
          IF (r > 1) THEN
             WRITE(*,'(A)',advance="no") ', '
          END IF
          WRITE (*,'(I0)',advance="no") expected_shape(r)
       END DO
       WRITE (*,'(A)',advance="no") "), actual: ("
       DO r = 1, rank
          IF (r > 1) THEN
             WRITE(*,'(A)',advance="no") ', '
          END IF
          WRITE (*,'(I0)',advance="no") actual_shape(r)
       END DO
       WRITE (*,'(A)') ")"
    END IF

  END FUNCTION cmp_size

  SUBROUTINE cmp_print_deviations_real_1d(expected, actual, fieldname_print, lbounds, t)
    REAL(wp), INTENT(IN)  :: expected(:), actual(:)
    CHARACTER(LEN=*), INTENT(IN) :: fieldname_print
    INTEGER, INTENT(IN), OPTIONAL :: lbounds(1)
    LOGICAL, ALLOCATABLE :: mask(:)
    REAL(wp), ALLOCATABLE :: deltas(:)
    INTEGER :: indices(1), indexAdj(1), expLbounds(1), expUbounds(1), i, j
    REAL, INTENT(in) :: t


    mask = .NOT. (actual /= actual .AND. expected /= expected) .AND. ABS(actual - expected) > t
    expLbounds = LBOUND(expected)
    expUbounds = UBOUND(expected)
    ALLOCATE(deltas(expLbounds(1):expUbounds(1)))
    deltas = ABS(expected - actual)
    IF (PRESENT(lbounds)) THEN
       indexAdj = lbounds
    ELSE
       indexAdj(:) = 1
    END IF

    DO i = 1, ftg_cmp_max_print_deviations
       IF (ANY(mask)) THEN
          indices = MAXLOC(deltas, mask)
          WRITE (*,'(A)',advance="no") "  -> ("
          WRITE (*,'(I0)',advance="no") indices(1) + indexAdj(1) - 1
          WRITE (*,'(A)',advance="no") "), expected: "
          WRITE (*,'(F0.14)',advance="no") expected(indices(1))
          WRITE (*,'(A)',advance="no") ", actual: "
          WRITE (*,'(F0.14)') actual(indices(1))
          mask(indices(1)) = .FALSE.
       ELSE
          EXIT
       END IF
    END DO

  END SUBROUTINE cmp_print_deviations_real_1d

  SUBROUTINE cmp_print_deviations_real_2d(expected, actual, fieldname_print, lbounds, t)
    REAL(wp), INTENT(IN)  :: expected(:,:), actual(:,:)
    CHARACTER(LEN=*), INTENT(IN) :: fieldname_print
    INTEGER, INTENT(IN), OPTIONAL :: lbounds(2)
    LOGICAL, ALLOCATABLE :: mask(:,:)
    REAL(wp), ALLOCATABLE :: deltas(:,:)
    INTEGER :: indices(2), indexAdj(2), expLbounds(2), expUbounds(2), i, j
    REAL, INTENT(in) :: t


    mask = .NOT. (actual /= actual .AND. expected /= expected) .AND. ABS(actual - expected) > t
    expLbounds = LBOUND(expected)
    expUbounds = UBOUND(expected)
    ALLOCATE(deltas(expLbounds(1):expUbounds(1), expLbounds(2):expUbounds(2)))
    deltas = ABS(expected - actual)
    IF (PRESENT(lbounds)) THEN
       indexAdj = lbounds
    ELSE
       indexAdj(:) = 1
    END IF

    DO i = 1, ftg_cmp_max_print_deviations
       IF (ANY(mask)) THEN
          indices = MAXLOC(deltas, mask)
          WRITE (*,'(A)',advance="no") "  -> ("
          WRITE (*,'(I0)',advance="no") indices(1) + indexAdj(1) - 1
          WRITE (*,'(A)',advance="no") ", "
          WRITE (*,'(I0)',advance="no") indices(2) + indexAdj(2) - 1
          WRITE (*,'(A)',advance="no") "), expected: "
          WRITE (*,'(F0.14)',advance="no") expected(indices(1), indices(2))
          WRITE (*,'(A)',advance="no") ", actual: "
          WRITE (*,'(F0.14)') actual(indices(1), indices(2))
          mask(indices(1), indices(2)) = .FALSE.
       ELSE
          EXIT
       END IF
    END DO

  END SUBROUTINE cmp_print_deviations_real_2d

  SUBROUTINE compare_real_1d(serializer, savepoint, fieldname, field, result, failure_count, lbounds, ubounds, &
       tolerance, fieldname_alias)
    TYPE(t_serializer), INTENT(IN), POINTER :: serializer
    TYPE(t_savepoint), INTENT(IN), POINTER :: savepoint
    CHARACTER(LEN=*), INTENT(IN) :: fieldname
    REAL(wp), INTENT(IN) :: field(:)
    LOGICAL, INTENT(OUT) :: result
    INTEGER, INTENT(INOUT), OPTIONAL :: failure_count
    CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: fieldname_alias
    CHARACTER(LEN=256) :: fieldname_print
    REAL(wp), ALLOCATABLE :: stored_field(:)
    INTEGER, INTENT(IN), OPTIONAL :: lbounds(1), ubounds(1)
    REAL, INTENT(in), OPTIONAL :: tolerance
    REAL :: t

    IF (PRESENT(tolerance)) THEN
       t = tolerance
    ELSE
       t = ftg_cmp_default_tolerance
    END IF

    IF (PRESENT(fieldname_alias)) THEN
       fieldname_print = fieldname_alias
    ELSE
       fieldname_print = fieldname
    END IF

    result = .TRUE.

    IF (.NOT. fs_field_exists(serializer, fieldname)) THEN
       IF (ftg_cmp_count_missing_field_as_failure) THEN
          result = .FALSE.
       END IF
       IF (.NOT. ftg_cmp_quiet) THEN
          WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : Don't exist in Serializer"
       END IF
    ELSE
       IF (.NOT. cmp_size(serializer, fieldname, SHAPE(field), fieldname_print)) THEN
          result = .FALSE.
       ELSE
          IF (PRESENT(lbounds) .AND. PRESENT(ubounds)) THEN
             IF (.NOT. cmp_bounds(serializer, fieldname, lbounds, ubounds, fieldname_print) .AND. &
                  ftg_cmp_count_different_bounds_as_failure) THEN
                result = .FALSE.
             END IF
          END IF
          CALL allocate_allocatable_real_1d(serializer, fieldname, stored_field)
          CALL fs_read_field(serializer, savepoint, fieldname, stored_field)
          IF (ANY(.NOT. (field /= field .AND. stored_field /= stored_field) .AND. ABS(field - stored_field) > t)) THEN
             result = .FALSE.
                WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : Not equal"
                IF (ftg_cmp_max_print_deviations > 0) THEN
                   CALL cmp_print_deviations_real_1d(stored_field, field, fieldname_print, lbounds, t)
                END IF
          END IF
       END IF
    END IF

    IF (result) THEN
!       IF (.NOT. ftg_cmp_quiet .AND. ftg_cmp_print_when_equal) THEN
          WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : OK"
!       END IF
    ELSE
       IF (PRESENT(failure_count)) THEN
          failure_count = failure_count + 1
       END IF
    END IF

  END SUBROUTINE compare_real_1d


  SUBROUTINE compare_real_2d(serializer, savepoint, fieldname, field, result, failure_count, lbounds, ubounds, &
       tolerance, fieldname_alias)
    TYPE(t_serializer), INTENT(IN), POINTER :: serializer
    TYPE(t_savepoint), INTENT(IN), POINTER :: savepoint
    CHARACTER(LEN=*), INTENT(IN) :: fieldname
    REAL(wp), INTENT(IN) :: field(:,:)
    LOGICAL, INTENT(OUT) :: result
    INTEGER, INTENT(INOUT), OPTIONAL :: failure_count
    CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: fieldname_alias
    CHARACTER(LEN=256) :: fieldname_print
    REAL(wp), ALLOCATABLE :: stored_field(:,:)
    INTEGER, INTENT(IN), OPTIONAL :: lbounds(2), ubounds(2)
    REAL, INTENT(in), OPTIONAL :: tolerance
    REAL :: t

    IF (PRESENT(tolerance)) THEN
       t = tolerance
    ELSE
       t = ftg_cmp_default_tolerance
    END IF
  
    IF (PRESENT(fieldname_alias)) THEN
       fieldname_print = fieldname_alias
    ELSE
       fieldname_print = fieldname
    END IF

    result = .TRUE.
    IF (.NOT. fs_field_exists(serializer, fieldname)) THEN
       IF (ftg_cmp_count_missing_field_as_failure) THEN
          result = .FALSE.
       END IF
       IF (.NOT. ftg_cmp_quiet) THEN
          WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : Don't exist in Serializer"
       END IF
    ELSE
       IF (.NOT. cmp_size(serializer, fieldname, SHAPE(field), fieldname_print)) THEN
          result = .FALSE.
       ELSE
          IF (PRESENT(lbounds) .AND. PRESENT(ubounds)) THEN
             IF (.NOT. cmp_bounds(serializer, fieldname, lbounds, ubounds, fieldname_print) .AND. &
                  ftg_cmp_count_different_bounds_as_failure) THEN
                result = .FALSE.
             END IF
          END IF
          CALL allocate_allocatable_real_2d(serializer, fieldname, stored_field)
          CALL fs_read_field(serializer, savepoint, fieldname, stored_field)
          IF (ANY(.NOT. (field /= field .AND. stored_field /= stored_field) .AND. ABS(field - stored_field) > t)) THEN
             result = .FALSE.
                WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : Not equal"
                IF (ftg_cmp_max_print_deviations > 0) THEN
                   CALL cmp_print_deviations_real_2d(stored_field, field, fieldname_print, lbounds, t)
                END IF
          END IF
       END IF
    END IF

    IF (result) THEN
!       IF (.NOT. ftg_cmp_quiet .AND. ftg_cmp_print_when_equal) THEN
          WRITE (*,'(A,A,A,A)') TRIM(ftg_cmp_message_prefix), " ", TRIM(fieldname_print), " : OK"
!       END IF
    ELSE
       IF (PRESENT(failure_count)) THEN
          failure_count = failure_count + 1
       END IF
    END IF

  END SUBROUTINE compare_real_2d

END PROGRAM data_checker
