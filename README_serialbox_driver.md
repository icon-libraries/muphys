# Graupel driver 

This repository also provides an executable for a standalone execution
of the granule routine by loading the necessary input data from an existing
Serialbox data set. The variables loaded from the input dataset are provided
to the graupel_run subroutine and the output data is saved in a new
Serialbox data set. Additionally, a second executable is provided to be used
to compare the dataset produced by the standalone granule execution
and a corresponding refference dataset, generated for example during a full
ICON run with active serialization.

# Building the serialbox_driver (example on CSCS machines)

Set the path to Serialbox, XML and fyaml installation path:

export SERIALBOX2_ROOT=<path_to_serialbox_install>

export XML2_ROOT=<path_to_xml2_install>

export FYAML_ROOT=<path_to_fyaml_install>

## Building on Daint with GCC and Intel compilers:

GCC:

module switch PrgEnv-cray PrgEnv-gnu

module load daint-gpu cray-netcdf

module load CMake

mkdir build_gcc_11.2.0

cd build_gcc_11.2.0

CC=cc FC=ftn cmake ..

Set the needed include/link option with path to Seriabox with ccmake

make

Intel:

module swap PrgEnv-cray PrgEnv-intel

module load daint-gpu cray-netcdf

module load CMake

mkdir build_intel_2021.3.0

cd build_intel_2021.3.0

CC=cc FC=ftn cmake ..

Set the needed include/link option with path to Seriabox with ccmake

make

# Serialbox dataset structure

The identification of the datasets used during the graupel standalone execution and
the run of the data comparison is based on Serialbox data naming, where the names
of data file for a given variable X and those of the metadata/archive files are:

<base_name>_X.dat

ArchiveMetaData-<base_name>.json

MetaData-<base_name>.json

During a driver run two base names are needed, one for the existing input dataset
and one for the output dataset that will be generated during the run itself. Two
base names are needed in case of a data comparison run, one corresponnding two a
"reference" dataset, for example created during a full ICON run with active
serialization, and one corresponging to a dataset created by a standalone granule
execution.

All the datasets are stored/expected to be in the same folder.

# Run

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<path_to_serialbox_install>/lib

Execution of the driver:

./driver <path_to_datasets_folder> <input_dataset_base_name> <output_dataset_base_name>

Execution of the data comparison:

./data_checker <path_to_datasets_folder> <reference_dataset_base_name> <new_dataset_base_name>


# Example

The example folder contains the data sets of a standalone (i.e. driver based) run of the granule
subroutine and another one produced during a full ICON run (with serialization) plus two .sh files containing the command to run the driver/data checker with these data sets.

# Preparing the gt4py-f2ser-serialbox toolchain for source code translation

In order to serialize real data from ICON, one needs the icon4py toolchain: 

```
  https://github.com/C2SM/icon4py
```

Follow instructions there to install.  

# Workflow for generating the serialbox data set from a real ICON run

We start with the original mo_aes_graupel.f90 from the icon-mpim repository. The workflow assumes that this file is *identical* to to the file of same name in this repository.

1. run gt4py f2ser and to get the graupel file with the !$SER directives 

```
python icon4py/tools/src/icon4pytools/f2ser/cli.py mo_aes_graupel.f90 mo_aes_graupel_f2ser.f90 # to add the !$SER directives
```

2. pp_ser on mo_aes_graupel_f2ser.f90 to generate serialization calls from the Serialbox library.

```
pp_ser.py mo_aes_graupel_f2ser.f90 > mo_aes_graupel_plugin.f90 2>&1 # to generate call s to serialbox
```

3. The resulting ```mo_aes_graupel_plugin.f90``` is a plug-in replacement for mo_aes_graupel.f90, albeit it assumes the availability of Serialbox to compile, link and run.  Insert the generated source file back in icon-mpim source tree and recompile with ```-DSERIALIZE``` run of icon-mpim to get the dataset for a specific iteration

4. Run icon-mpim with the configuration which calls Graupel. The serialization mode ensures that the data coming into and out of ```graupel_run``` are serialized with Serialbox.

Assuming mo_aes_graupel.f90 is the same both in this repository and in icon-mpim, one can now compile and run the standalone as described above with the serialized data. The standalone serialized output can then me compared against the output dumped in the last step.
