PROGRAM driver
  USE netcdf
  USE mo_kind
  USE mo_aes_graupel, ONLY : graupel_init, graupel_run, graupel_finalize

  IMPLICIT NONE

  ! Parameters from the command line
  CHARACTER(LEN=:), ALLOCATABLE :: input_file
  INTEGER :: itime
  REAL(wp) :: dt, qnc
  REAL(wp) :: qnc_(1)

  ! Parameters from the input file
  INTEGER :: ncells, nlev
  REAL(wp), DIMENSION(:, :), ALLOCATABLE :: z, t, p, rho, qv, qc, qi, qr, qs, qg

  ! Precalculated parameters
  REAL(wp), DIMENSION(:, :), ALLOCATABLE :: dz

  ! Extra fields required to call graupel
  REAL(wp), DIMENSION(:, :), ALLOCATABLE :: pflx
  REAL(wp), DIMENSION(:), ALLOCATABLE :: prr_gsp, pri_gsp, prs_gsp, prg_gsp

  ! Start/end indices
  INTEGER :: kend, kbeg, ivend, ivbeg, nvec, i

  ! Timer variables
  INTEGER(i8) :: start_count, end_count, count_rate

  ! variable to get the coordinates from
  character(len=2), parameter :: BASEVAR = "zg"
  ! value of possibly given environment variable OUTPUT_CF
  character(len=4) :: env_value
  character(len=4), dimension(5), parameter :: ENV_POSITIVES = ['yes ', 'true', 'y   ', 't   ', '0   ']
  logical :: output_cf = .false.

  CALL parse_args(input_file, itime, dt, qnc)

  CALL read_fields(input_file, itime, ncells, nlev, z, t, p, rho, qv, qc, qi, qr, qs, qg)

  dz = calc_dz(z, ncells, nlev)
  ALLOCATE(prr_gsp(ncells)); prr_gsp = 0
  ALLOCATE(pri_gsp(ncells)); pri_gsp = 0
  ALLOCATE(prs_gsp(ncells)); prs_gsp = 0
  ALLOCATE(prg_gsp(ncells)); prg_gsp = 0
  ALLOCATE(pflx(ncells, nlev)); pflx = 0

  kbeg = 1; kend = nlev
  ivbeg = 1; ivend = ncells
  nvec = ncells
  qnc_(1) = qnc

  ! Uncomment if needed
  ! CALL write_fields("old_fields.nc", ncells, nlev, t, qv, qc, qi, qr, qs, qg)

  CALL graupel_init()

  !$acc data &
  !$acc copy(dz, t, p, rho, qv, qc, qi, qr, qs, qg, qnc_) &
  !$acc copy(prr_gsp, prs_gsp, pri_gsp, prg_gsp, pflx)

  CALL system_clock(start_count, count_rate)

  CALL graupel_run(nvec, kend, ivbeg, ivend, kbeg, &
             & dt, dz, t, p, rho, qv, qc, qi, qr, qs, qg, qnc_, &
             & prr_gsp, pri_gsp, prs_gsp, prg_gsp, pflx)

  CALL system_clock(end_count)

  !$acc end data

  write(*, "(a,f0.2,a)") "Wall clock time: ", 1000.0 * REAL(end_count - start_count) / REAL(count_rate), " ms"

  ! write cf in case one of the possible values is given
  call get_environment_variable("OUTPUT_CF", env_value)
  do i=1,size(ENV_POSITIVES)
    if ( (trim(ENV_POSITIVES(i)) == trim(env_value)) ) then
      output_cf = .true.
      exit
    endif
  enddo

  if ( .true. .eqv. output_cf ) then
    CALL write_cf_fields("new_fields.nc", input_file, ncells, nlev, t, qv, qc, qi, qr, qs, qg)
  else
    CALL write_fields("new_fields.nc", ncells, nlev, t, qv, qc, qi, qr, qs, qg)
  endif

  CALL graupel_finalize()

CONTAINS

  SUBROUTINE parse_args(input_file, itime, dt, qnc)
    CHARACTER(LEN=:), ALLOCATABLE, INTENT(OUT) :: input_file
    INTEGER, INTENT(OUT) :: itime
    REAL(wp), INTENT(OUT) :: dt, qnc

    CHARACTER(LEN=:), ALLOCATABLE :: buf
    INTEGER :: nargs, arg_len

    nargs = command_argument_count()

    IF (nargs > 0) THEN
      CALL get_command_argument(1, length=arg_len)
      ALLOCATE(CHARACTER(arg_len) :: input_file)
      CALL get_command_argument(1, input_file)
    ELSE
      call stop_on_error('Missing input filename', &
        &                 __FILE__, &
        &                 __LINE__)
    END IF
    WRITE(*, '(a, a)') "input_file: ", input_file

    IF (nargs > 1) THEN
      CALL get_command_argument(2, length=arg_len)
      ALLOCATE(CHARACTER(arg_len) :: buf)
      CALL get_command_argument(2, buf)
      READ(buf, *) itime
      DEALLOCATE(buf)
    ELSE
      itime = 1
    END IF
    WRITE(*, '(a, i0)') "itime: ", itime

    IF (nargs > 2) THEN
      CALL get_command_argument(3, length=arg_len)
      ALLOCATE(CHARACTER(arg_len) :: buf)
      CALL get_command_argument(3, buf)
      READ(buf, *) dt
      DEALLOCATE(buf)
    ELSE
      dt = 30.0
    END IF
    WRITE(*, '(a, f0.1)') "dt: ", dt

    IF (nargs > 3) THEN
      CALL get_command_argument(4, length=arg_len)
      ALLOCATE(CHARACTER(arg_len) :: buf)
      CALL get_command_argument(4, buf)
      READ(buf, *) qnc
      DEALLOCATE(buf)
    ELSE
      qnc = 100.0
    END IF
    WRITE(*, '(a, f0.1)') "qnc: ", qnc

  END SUBROUTINE parse_args

  FUNCTION calc_dz(z, ncells, nlev)
    REAL(wp), DIMENSION(:, :), INTENT(IN) :: z
    INTEGER, INTENT(IN) :: ncells, nlev
    REAL(wp), DIMENSION(ncells, nlev) :: calc_dz

    REAL(wp), DIMENSION(ncells, nlev + 1) :: zh
    INTEGER :: k

    zh(:, nlev + 1) = (3.*z(:,nlev)-z(:,nlev-1))*0.5
    DO k = nlev, 1, -1
      zh(:, k) = 2.0 * z(:, k) - zh(:, k + 1)
      calc_dz(:, k) = -zh(:, k + 1) + zh(:, k)
    END DO
  END FUNCTION calc_dz

  SUBROUTINE read_fields(filename, itime, ncells, nlev, z, t, p, rho, qv, qc, qi, qr, qs, qg)
    CHARACTER(LEN=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: itime
    INTEGER, INTENT(OUT) :: ncells, nlev
    REAL(wp), DIMENSION(:, :), ALLOCATABLE :: z, t, p, rho, qv, qc, qi, qr, qs, qg


    integer, dimension(nf90_max_var_dims) :: DimIds
    INTEGER :: ncid, numdims, varid, numatts
    character(len = nf90_max_name) :: DimName

    CALL handle_nc_code(nf90_open(filename, NF90_NOWRITE, ncid), lineno=__LINE__)

    ! get base variable to inq dimensions
    CALL handle_nc_code(nf90_inq_varid(ncid, BASEVAR, varid), lineno=__LINE__)
    call handle_nc_code(nf90_inquire_variable(ncid, varid, ndims = numdims, natts = numatts))
    CALL handle_nc_code(nf90_inquire_variable(ncid, varid, dimids = dimids(:)))
    call handle_nc_code(nf90_inquire_dimension(ncid, DimIds(1), name = dimname, len = ncells))
    call handle_nc_code(nf90_inquire_dimension(ncid, DimIds(2), name = dimname, len = nlev))

    z = read_nc_2d_field(ncid, "zg", ncells, nlev)
    t = read_nc_2d_field(ncid, "ta", ncells, nlev, start=[1, 1, itime])
    p = read_nc_2d_field(ncid, "pfull", ncells, nlev, start=[1, 1, itime])
    rho = read_nc_2d_field(ncid, "rho", ncells, nlev, start=[1, 1, itime])
    qv = read_nc_2d_field(ncid, "hus", ncells, nlev, start=[1, 1, itime])
    qc = read_nc_2d_field(ncid, "clw", ncells, nlev, start=[1, 1, itime])
    qi = read_nc_2d_field(ncid, "cli", ncells, nlev, start=[1, 1, itime])
    qr = read_nc_2d_field(ncid, "qr", ncells, nlev, start=[1, 1, itime])
    qs = read_nc_2d_field(ncid, "qs", ncells, nlev, start=[1, 1, itime])
    qg = read_nc_2d_field(ncid, "qg", ncells, nlev, start=[1, 1, itime])
    CALL handle_nc_code(nf90_close(ncid), lineno=__LINE__)
  END SUBROUTINE read_fields

  ! write 3d fields without any meta data
  SUBROUTINE write_fields(filename, ncells, nlev, t, qv, qc, qi, qr, qs, qg, deflate_level)
    CHARACTER(LEN=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: ncells, nlev
    REAL(wp), DIMENSION(ncells, nlev), INTENT(in) :: t, qv, qc, qi, qr, qs, qg
    INTEGER, OPTIONAL :: deflate_level

    INTEGER :: ncid, ncells_dimid, nlev_dimid, dimids(2)
    INTEGER :: t_varid, qv_varid, qc_varid, qi_varid, qr_varid, qs_varid, qg_varid

    CALL handle_nc_code(nf90_create(TRIM(filename), NF90_NETCDF4, ncid), lineno=__LINE__)

    CALL handle_nc_code(nf90_def_dim(ncid, "ncells", ncells, ncells_dimid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_dim(ncid, "height", nlev, nlev_dimid), lineno=__LINE__)
    dimids = [ncells_dimid, nlev_dimid]

    CALL handle_nc_code(nf90_def_var(ncid, "ta", NF90_DOUBLE, dimids, t_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_var(ncid, "hus", NF90_DOUBLE, dimids, qv_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_var(ncid, "clw", NF90_DOUBLE, dimids, qc_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_var(ncid, "cli", NF90_DOUBLE, dimids, qi_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_var(ncid, "qr", NF90_DOUBLE, dimids, qr_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_var(ncid, "qs", NF90_DOUBLE, dimids, qs_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_def_var(ncid, "qg", NF90_DOUBLE, dimids, qg_varid), lineno=__LINE__)
    IF (PRESENT(deflate_level)) THEN
      CALL handle_nc_code(nf90_def_var_deflate(ncid, t_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qv_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qc_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qi_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qr_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qs_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qg_varid, 0, 1, deflate_level), lineno=__LINE__)
    ENDIF
    CALL handle_nc_code(nf90_enddef(ncid), lineno=__LINE__)


    CALL handle_nc_code(nf90_put_var(ncid, t_varid, t), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qv_varid, qv), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qc_varid, qc), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qi_varid, qi), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qr_varid, qr), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qs_varid, qs), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qg_varid, qg), lineno=__LINE__)
    CALL handle_nc_code(nf90_close(ncid), lineno=__LINE__)
  END SUBROUTINE write_fields

  ! copy given attributes from one variable to another
  subroutine copy_var_attributes_if_present(attribute_list, varname, ifileID, ofileID, ovarID)
    integer, intent(in) :: ifileID, ofileID, ovarID
    character(len=*), intent(in) ::  attribute_list(:)
    character(len=*), intent(in)  :: varname
    integer input_varid, i, code

    call handle_nc_code(nf90_inq_varid(ifileID, varname, input_varid), lineno=__LINE__)
    do i=1,size(attribute_list)
      ! copy if available
      code = nf90_inquire_attribute(ifileID, input_varid, trim(attribute_list(i)))
      IF ( code /= NF90_NOERR) THEN
        !DEBUG print *, 'Attribute ', trim(attribute_list(i)), ' not found: SKIP'
        cycle
      ELSE
        call handle_nc_code(nf90_copy_att(ifileID, input_varid, trim(attribute_list(i)), ofileID, ovarID), lineno=__LINE__)
      ENDIF
    enddo
  end subroutine copy_var_attributes_if_present

  ! check for exiting dimension in file given by file handle ID
  logical function has_dim(fileID, dimname)
    integer, intent(in) :: fileID
    character(len=*), intent(in) :: dimname

    integer :: status, dimid

    has_dim = .false.

    status = nf90_inq_dimid(fileID, dimname, dimid)
    if (status == nf90_noerr) then
      has_dim = .true.
    endif
  end function

  ! check for exiting variable in file given by file handle ID
  logical function has_var(fileID, varname)
    integer, intent(in) :: fileID
    character(len=*), intent(in) :: varname

    integer :: status, dimid

    has_var = .false.

    status = nf90_inq_varid(fileID, varname, dimid)
    if (status == nf90_noerr) then
      has_var = .true.
    endif
  end function

  ! copy a list of variables given by shortname from input to output given by
  ! netcdf file handles
  SUBROUTINE copy_variables_if_present(variable_list, ifileID, ofileID)
    character(len=*), intent(in) :: variable_list(:)
    integer, intent(in) :: ifileID, ofileID

    integer :: i, j, myVarid, newVarid
    integer :: numDims, numAtts
    integer, dimension(nf90_max_var_dims) :: dimids, dimlens, newdimids
    integer                               :: xtype, len, attnum
    integer :: code

    real, dimension(:, :), allocatable :: values_2d
    real, dimension(:),    allocatable :: values_1d

    character(len=20) :: name, values_char

    DO i=1, size(variable_list)
      ! get the varid
      code = nf90_inq_varid(ifileID,trim(adjustl(variable_list(i))),myVarid)
      !, lineno=__LINE__)
      IF (code /= NF90_NOERR) THEN
        !DEBUG print *,'cound not copy varable: ',trim(adjustl(variable_list(i))), ' ... SKIP'
        cycle
      ENDIF

      ! get dimensions and attributes of this variable and add it in case it is not already in the output file
      call handle_nc_code(&
        & nf90_inquire_variable(ifileID, myVarid, dimids = dimids, ndims = numDims, natts = numAtts),&
        & lineno=__LINE__)

      ! get the dims of the variable
      DO j=1,numDims
        call handle_nc_code(nf90_inquire_dimension(ifileID, dimids(j), name=name, len=dimlens(j)), lineno=__LINE__)

        if (.not. has_dim(ofileID,trim(adjustl(name)))) then
          CALL handle_nc_code( nf90_def_dim(ofileID, name, dimlens(j), newdimids(j)), lineno=__LINE__)
        else
          call handle_nc_code( nf90_inq_dimid(ofileID, name, newdimids(j)), lineno=__LINE__)
        endif
      ENDDO
      ! define new variable in the output
      CALL handle_nc_code( &
        & nf90_def_var(ofileID, trim(adjustl(variable_list(i))), NF90_DOUBLE, newdimids(1:numDims), newVarid),&
        & lineno=__LINE__)
      ! copy their data
      select case (numDims)
      case (1)
        allocate(values_1d(dimlens(1)))
        call handle_nc_code(nf90_get_var(ifileID, myVarid, values_1d))
        call handle_nc_code(nf90_enddef(ofileID))
        call handle_nc_code(nf90_put_var(ofileID, newVarid, values_1d), lineno=__LINE__)
        call handle_nc_code(nf90_redef(ofileID))
        deallocate(values_1d)
      case (2)
        allocate(values_2d(dimlens(1), dimlens(2)))
        call handle_nc_code(nf90_get_var(ifileID, myVarid, values_2d))
        call handle_nc_code(nf90_enddef(ofileID))
        call handle_nc_code(nf90_put_var(ofileID, newVarid, values_2d), lineno=__LINE__)
        call handle_nc_code(nf90_redef(ofileID))
        deallocate(values_2d)
      case default
        print *,'Found variable with too many dimensions ',trim(adjustl(variable_list(i)))
      end select

      ! copy their attributes
      do j=1,numAtts
        call handle_nc_code( nf90_inq_attname(ifileID, myVarid, j, name), lineno=__LINE__)
        call handle_nc_code( nf90_inquire_attribute(ifileID, myVarid, name, xtype, len, attnum), lineno=__LINE__)
        ! string attributes, only
        if ( .not. NF90_CHAR.eq.xtype ) continue
        call handle_nc_code(nf90_get_att(ifileID, myVarid, trim(adjustl(name)), values_char))
        call handle_nc_code(nf90_put_att(ofileID, newVarid, trim(adjustl(name)), values_char))
      enddo
    ENDDO
  END SUBROUTINE copy_variables_if_present

  ! write output in a cf-conform way:
  ! - copy attributes and coordinate
  ! - take over dimension names from the input
  ! This should allow later processing with tools like CDO esp. to identify
  ! vertical levels and allow horizontal interpolation
  SUBROUTINE write_cf_fields(filename, inputfilename, ncells, nlev, t, qv, qc, qi, qr, qs, qg, deflate_level)
    CHARACTER(LEN=*), INTENT(IN) :: filename, inputfilename
    INTEGER, INTENT(IN) :: ncells, nlev
    REAL(wp), DIMENSION(ncells, nlev), INTENT(in) :: t, qv, qc, qi, qr, qs, qg
    INTEGER, OPTIONAL :: deflate_level

    INTEGER :: ncid, ncells_dimid, nlev_dimid, dimids(2), input_ncid, input_xtype, input_ndims
    INTEGER :: t_varid, qv_varid, qc_varid, qi_varid, qr_varid, qs_varid, qg_varid, input_varid
    CHARACTER(LEN=20) :: dummy, dimnames(2)
    INTEGER :: i, dimlen, j


    CALL handle_nc_code(nf90_create(TRIM(filename), NF90_NETCDF4, ncid), lineno=__LINE__)

    ! re-open the input file for copy coordinates and variable attributes
    CALL handle_nc_code(nf90_open(trim(inputfilename), nf90_nowrite, input_ncid), lineno=__LINE__)
    ! use horizontal and vertical dimensions definded in the input file on the BASEVAR
    CALL handle_nc_code(nf90_inq_varid(input_ncid, BASEVAR, input_varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_inquire_variable(input_ncid, input_varid, dummy, input_xtype, input_ndims, dimids), lineno=__LINE__)
    ! copy input dimensions to output file
    DO i=1,SIZE(dimids)
      !print *,'I=',i,' dimifs(',i,') = ',dimids(i)
      j = nf90_inquire_dimension(input_ncid, dimids(i), dummy, dimlen)
      dimnames(i) = dummy
      !print *,'Found input dim: ',trim(dummy)
      CALL handle_nc_code(nf90_def_dim(ncid, dummy, dimlen, dimids(i)), lineno=__LINE__)
    ENDDO
    ncells_dimid = dimids(1)
    nlev_dimid = dimids(2)

    ! copy the cordinate variables including their attributes
    call copy_variables_if_present((/"        clon", &
      &                   "   clon_bnds", &
      &                   "        clat", &
      &                   "   clat_bnds", &
      &                trim(dimnames(2)), &
      &                   " height_bnds"/), &
      & input_ncid, ncid);


    CALL handle_nc_code(nf90_def_var(ncid, "ta", NF90_DOUBLE, dimids, t_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"ta", &
                                        & input_ncid, ncid, t_varid)
    CALL handle_nc_code(nf90_def_var(ncid, "hus", NF90_DOUBLE, dimids, qv_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"hus", &
                                        & input_ncid, ncid, qv_varid)
    CALL handle_nc_code(nf90_def_var(ncid, "clw", NF90_DOUBLE, dimids, qc_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"clw", &
                                        & input_ncid, ncid, qc_varid)
    CALL handle_nc_code(nf90_def_var(ncid, "cli", NF90_DOUBLE, dimids, qi_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"cli", &
                                        & input_ncid, ncid, qi_varid)
    CALL handle_nc_code(nf90_def_var(ncid, "qr", NF90_DOUBLE, dimids, qr_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"qr", &
                                        & input_ncid, ncid, qr_varid)
    CALL handle_nc_code(nf90_def_var(ncid, "qs", NF90_DOUBLE, dimids, qs_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"qs", &
                                        & input_ncid, ncid, qs_varid)
    CALL handle_nc_code(nf90_def_var(ncid, "qg", NF90_DOUBLE, dimids, qg_varid), lineno=__LINE__)
    call copy_var_attributes_if_present((/"coordinates  ","units        ","long_name    ","standard_name"/),"qg", &
                                        & input_ncid, ncid, qg_varid)

    IF (PRESENT(deflate_level)) THEN
      CALL handle_nc_code(nf90_def_var_deflate(ncid, t_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qv_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qc_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qi_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qr_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qs_varid, 0, 1, deflate_level), lineno=__LINE__)
      CALL handle_nc_code(nf90_def_var_deflate(ncid, qg_varid, 0, 1, deflate_level), lineno=__LINE__)
    ENDIF
    CALL handle_nc_code(nf90_enddef(ncid), lineno=__LINE__)

    ! close input file
    CALL handle_nc_code(nf90_close(input_ncid), lineno=__LINE__)

    CALL handle_nc_code(nf90_put_var(ncid, t_varid, t), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qv_varid, qv), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qc_varid, qc), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qi_varid, qi), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qr_varid, qr), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qs_varid, qs), lineno=__LINE__)
    CALL handle_nc_code(nf90_put_var(ncid, qg_varid, qg), lineno=__LINE__)
    CALL handle_nc_code(nf90_close(ncid), lineno=__LINE__)
  END SUBROUTINE write_cf_fields

  FUNCTION get_nc_dim_size(ncid, dimname)
    INTEGER, INTENT(IN) :: ncid
    CHARACTER(LEN=*), INTENT(IN) :: dimname
    INTEGER :: get_nc_dim_size

    INTEGER :: dimid

    CALL handle_nc_code(nf90_inq_dimid(ncid, TRIM(dimname), dimid), lineno=__LINE__)
    CALL handle_nc_code(nf90_inquire_dimension(ncid, dimid, len=get_nc_dim_size), lineno=__LINE__)
  END FUNCTION get_nc_dim_size

  FUNCTION read_nc_2d_field(ncid, varname, nx, ny, start)
    INTEGER, INTENT(IN) :: ncid
    CHARACTER(LEN=*), INTENT(IN) :: varname
    INTEGER, INTENT(IN) :: nx, ny
    INTEGER, DIMENSION(:), OPTIONAL, INTENT(IN) :: start
    REAL(wp), DIMENSION(nx, ny) :: read_nc_2d_field

    INTEGER :: varid

    CALL handle_nc_code(nf90_inq_varid(ncid, TRIM(varname), varid), lineno=__LINE__)
    CALL handle_nc_code(nf90_get_var(ncid, varid, read_nc_2d_field, start=start), lineno=__LINE__)
  END FUNCTION read_nc_2d_field

  SUBROUTINE handle_nc_code(code, filename, lineno)
    INTEGER, INTENT(IN) :: code
    CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: filename
    INTEGER, OPTIONAL, INTENT(IN) :: lineno

    IF (code /= NF90_NOERR) &
      CALL stop_on_error(nf90_strerror(code), filename, lineno)
  END SUBROUTINE handle_nc_code

  SUBROUTINE stop_on_error(msg, filename, lineno)
    USE iso_fortran_env, ONLY: error_unit
    CHARACTER(LEN=*), INTENT(IN) :: msg
    CHARACTER(LEN=*), OPTIONAL, INTENT(IN) :: filename
    INTEGER, OPTIONAL, INTENT(IN) :: lineno

    CHARACTER(LEN=:), ALLOCATABLE :: fn
    CHARACTER(LEN=100) :: ln

    IF (PRESENT(filename)) THEN
      fn = TRIM(filename) // ":"
    ELSE
      fn = __FILE__ // ":"
    END IF
    IF (PRESENT(lineno)) WRITE(ln, "(i0,a)") lineno, ":"

    IF (LEN_TRIM(msg) > 0) &
      WRITE(error_unit, "(a)") fn // TRIM(ln) // " " //TRIM(msg)
    ERROR STOP 1
  END SUBROUTINE stop_on_error

END PROGRAM driver 
