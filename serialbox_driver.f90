PROGRAM driver

  USE mo_kind
  USE mo_driver_utils
  USE m_serialize, ONLY : t_serializer, t_savepoint, fs_create_serializer, fs_destroy_serializer, &
       fs_create_savepoint, fs_destroy_savepoint, fs_read_field, fs_write_field, fs_get_halos
  USE mo_aes_graupel, ONLY : graupel_init, graupel_run, graupel_finalize
  
  IMPLICIT NONE

  ! Parameters from the command line
  CHARACTER(LEN=:), ALLOCATABLE :: directory, base_name_in, base_name_out

  ! Fields loaded (size+content) from input data files (directory/base_name_in/graupel_run_in)
  REAL(wp), ALLOCATABLE :: dz(:, :), t(:, :), p(:, :), rho(:, :), qv(:, :), qc(:, :), qi(:, :), qr(:, :), qs(:, :), qg(:, :)
  REAL(wp), ALLOCATABLE :: qnc(:)
  REAL(wp) :: dt
  INTEGER :: nvec, kend, kbeg, ivend, ivbeg

  ! Fields loaded (size only needed) from input data files (directory/base_name_in/graupel_run_out)
  REAL(wp), ALLOCATABLE :: pflx(:, :)
  REAL(wp), ALLOCATABLE :: prr_gsp(:), pri_gsp(:), prs_gsp(:), prg_gsp(:)

  ! Serialbox variables
  TYPE(t_serializer), POINTER :: serializer
  TYPE(t_savepoint), POINTER :: savepoint
  INTEGER, DIMENSION(4) :: field_size
  INTEGER :: tmp_bounds(8), &
       lbounds_prr_gsp(1), ubounds_prr_gsp(1), &
       lbounds_pri_gsp(1), ubounds_pri_gsp(1), &
       lbounds_prs_gsp(1), ubounds_prs_gsp(1), &
       lbounds_prg_gsp(1), ubounds_prg_gsp(1), &
       lbounds_pflx(2), ubounds_pflx(2)

  LOGICAL :: error_status
  error_status = .FALSE.
  CALL parse_args(directory, base_name_in, base_name_out, error_status)
  IF(error_status) THEN
     RETURN
  END IF

  !!! SERIALIZED DATA LOADING

  PRINT*,'Loading ',base_name_in,' dataset'

  ALLOCATE(serializer)
  ALLOCATE(savepoint)
  CALL fs_create_serializer(directory, base_name_in, 'r', serializer)
  CALL fs_create_savepoint('graupel_run_in', savepoint)
  CALL fs_read_field(serializer, savepoint, 'nvec', nvec)
  CALL fs_read_field(serializer, savepoint, 'ke', kend)
  CALL fs_read_field(serializer, savepoint, 'kstart', kbeg)
  CALL fs_read_field(serializer, savepoint, 'ivend', ivend)
  CALL fs_read_field(serializer, savepoint, 'ivstart', ivbeg)
  CALL fs_read_field(serializer, savepoint, 'dt', dt) 
  CALL allocate_allocatable_real_2d(serializer, 'dz', dz)
  CALL fs_read_field(serializer, savepoint, 'dz', dz)
  CALL allocate_allocatable_real_2d(serializer, 't', t)
  CALL fs_read_field(serializer, savepoint, 't', t)
  CALL allocate_allocatable_real_2d(serializer, 'p', p)
  CALL fs_read_field(serializer, savepoint, 'p', p)
  CALL allocate_allocatable_real_2d(serializer, 'rho', rho)
  CALL fs_read_field(serializer, savepoint, 'rho', rho)
  CALL allocate_allocatable_real_2d(serializer, 'qv', qv)
  CALL fs_read_field(serializer, savepoint, 'qv', qv)
  CALL allocate_allocatable_real_2d(serializer, 'qc', qc)
  CALL fs_read_field(serializer, savepoint, 'qc', qc)
  CALL allocate_allocatable_real_2d(serializer, 'qi', qi)
  CALL fs_read_field(serializer, savepoint, 'qi', qi)
  CALL allocate_allocatable_real_2d(serializer, 'qr', qr)
  CALL fs_read_field(serializer, savepoint, 'qr', qr)
  CALL allocate_allocatable_real_2d(serializer, 'qs', qs)
  CALL fs_read_field(serializer, savepoint, 'qs', qs)
  CALL allocate_allocatable_real_2d(serializer, 'qg', qg)
  CALL fs_read_field(serializer, savepoint, 'qg', qg)
  CALL allocate_allocatable_real_1d(serializer, 'qnc', qnc)
  CALL fs_read_field(serializer, savepoint, 'qnc', qnc)

  CALL fs_destroy_savepoint(savepoint)
  CALL fs_create_savepoint('graupel_run_out', savepoint)
  CALL allocate_allocatable_real_2d(serializer, 'pflx', pflx)
  tmp_bounds = fs_get_halos(serializer, 'pflx')
  lbounds_pflx(1) = tmp_bounds(1)
  ubounds_pflx(1) = tmp_bounds(2)
  lbounds_pflx(2) = tmp_bounds(3)
  ubounds_pflx(2) = tmp_bounds(4)
  CALL allocate_allocatable_real_1d(serializer, 'prr_gsp', prr_gsp)
  tmp_bounds = fs_get_halos(serializer, 'prr_gsp')
  lbounds_prr_gsp(1) = tmp_bounds(1)
  ubounds_prr_gsp(1) = tmp_bounds(2)
  CALL allocate_allocatable_real_1d(serializer, 'pri_gsp', pri_gsp)
  tmp_bounds = fs_get_halos(serializer, 'pri_gsp')
  lbounds_pri_gsp(1) = tmp_bounds(1)
  ubounds_pri_gsp(1) = tmp_bounds(2)
  CALL allocate_allocatable_real_1d(serializer, 'prs_gsp', prs_gsp)
  tmp_bounds = fs_get_halos(serializer, 'prs_gsp')
  lbounds_prs_gsp(1) = tmp_bounds(1)
  ubounds_prs_gsp(1) = tmp_bounds(2)
  CALL allocate_allocatable_real_1d(serializer, 'prg_gsp', prg_gsp)
  tmp_bounds = fs_get_halos(serializer, 'prg_gsp')
  lbounds_prg_gsp(1) = tmp_bounds(1)
  ubounds_prg_gsp(1) = tmp_bounds(2)
  CALL fs_destroy_serializer(serializer)
  CALL fs_destroy_savepoint(savepoint)

  !!! GRAUPEL RUN

  PRINT*,'Running graupel'

  CALL graupel_init()

  CALL graupel_run(nvec, kend, ivbeg, ivend, kbeg, &
             & dt, dz, t, p, rho, qv, qc, qi, qr, qs, qg, qnc, &
             & prr_gsp, pri_gsp, prs_gsp, prg_gsp, pflx)

  CALL graupel_finalize()

  !!! SERIALIZED DATA WRITING

  PRINT*,'Writing ',base_name_out,' dataset'

  ! TODO: check this step! In ICON code we write  without providing
  ! any lower/upper bound to the fs_write function optional parameters for
  ! the bounds (halo?). So it should be safe to assume that we can call
  ! ftg_write with zero halos. I'm retrieving the halos anyway...
  CALL fs_create_serializer(directory, base_name_out, 'w', serializer)
  CALL fs_create_savepoint('graupel_standalone_run_out', savepoint)  
  CALL fs_write_field(serializer, savepoint, "pflx", pflx, lbounds_pflx, ubounds_pflx)
  CALL fs_write_field(serializer, savepoint, "prr_gsp", prr_gsp, lbounds_prr_gsp, ubounds_prr_gsp)
  CALL fs_write_field(serializer, savepoint, "pri_gsp", pri_gsp, lbounds_pri_gsp, ubounds_pri_gsp)
  CALL fs_write_field(serializer, savepoint, "prs_gsp", prs_gsp, lbounds_prs_gsp, ubounds_prs_gsp)
  CALL fs_write_field(serializer, savepoint, "prg_gsp", prg_gsp, lbounds_prg_gsp, ubounds_prg_gsp)

  CALL fs_destroy_serializer(serializer)
  CALL fs_destroy_savepoint(savepoint)
    
CONTAINS

  SUBROUTINE parse_args(data_directory, base_name_in, base_name_out, error_status)
    CHARACTER(LEN=:), ALLOCATABLE, INTENT(OUT) :: data_directory, base_name_in, base_name_out
    INTEGER :: nargs, arg_len
    LOGICAL, INTENT(INOUT) :: error_status
    
    nargs = command_argument_count()

    IF (nargs < 3) THEN
       PRINT*, "Error, data directory and/or base names not provided. The driver execution command is:"
       PRINT*, "./graupel_driver <path_to_datasets_folder> <input_dataset_base_name> <output_dataset_base_name>"
       error_status = .TRUE.
       RETURN
    END IF
    
    CALL get_command_argument(1, length=arg_len)
    ALLOCATE(CHARACTER(arg_len) :: data_directory)
    CALL get_command_argument(1, data_directory)
    WRITE(*, '(a, a)') "data directory: ", data_directory

    CALL get_command_argument(2, length=arg_len)
    ALLOCATE(CHARACTER(arg_len) :: base_name_in)
    CALL get_command_argument(2, base_name_in)
    WRITE(*, '(a, a)') "base_name_in: ", base_name_in

    CALL get_command_argument(3, length=arg_len)
    ALLOCATE(CHARACTER(arg_len) :: base_name_out)
    CALL get_command_argument(3, base_name_out)
    WRITE(*, '(a, a)') "base_name_out: ", base_name_out

  END SUBROUTINE parse_args
  
END PROGRAM driver
