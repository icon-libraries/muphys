```bash
# Building on Levante with Intel compiler:

module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0
cmake -DCMAKE_Fortran_COMPILER=ifort -D CMAKE_Fortran_FLAGS='-m64 -march=core-avx2 -mtune=core-avx2 -g -gdwarf-4 -pc64 -fp-model source -O3 -ftz -qoverride-limits -assume realloc_lhs -align array64byte -fma -ip'  -DNetCDF_ROOT=/sw/spack-levante/netcdf-fortran-4.5.3-k6xq5g .
make -j

# Manual run on the login node:
./graupel_driver <input_file>

# For CF-configorm output (positive: yes, true, y, t, 0; negative: anything else or unset)
OUTPUT_CF=y ./graupel_driver <input_file>

# Run on the compute node via SLURM:
sbatch < levante.sbatch

# Building on Daint with GCC and Intel compilers: this is simpler than the above, since we make use of Cray's modules and ignore optimizations in the flags.

GCC

module switch PrgEnv-cray PrgEnv-gnu
module load daint-gpu cray-netcdf
mkdir build_gcc_11.2.0
cd build_gcc_11.2.0
CC=cc FC=ftn cmake ..
make -j
./graupel_driver <input_file>

(e.g., ./graupel /project/csstaff/inputs/icon/pool/data/ICON/muphys/aes-new-gr_moderate-dt30s_atm_3d_ml_20080801T000000Z.nc )

Intel:

module swap PrgEnv-cray PrgEnv-intel
module load daint-gpu cray-netcdf
mkdir build_intel_2021.3.0
cd build_intel_2021.3.0
CC=cc FC=ftn cmake ..
make -j 
./graupel_driver <input_file>

```

Examples of `<input_file>`s can be found [here](https://swiftbrowser.dkrz.de/public/dkrz_f23c4ba9-4a6c-4d70-8739-7bca74b37234/muphys_data/).
